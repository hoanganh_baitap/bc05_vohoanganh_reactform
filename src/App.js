import logo from './logo.svg';
import './App.css';
import ThongTinSinhVien from './ReactJsForm/ThongTinSinhVien';
import TableSinhVien from './ReactJsForm/TableSinhVien';


function App() {
  return (
    <div className="App">
      <ThongTinSinhVien />
      <TableSinhVien />
    </div>
  );
}

export default App;
