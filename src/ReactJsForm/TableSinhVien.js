import React, { Component } from 'react'
import { connect } from 'react-redux'

class TableSinhVien extends Component {
  // render table sinh vien
  renderTable = ()=>{
    return this.props.mangSV.map((sv, index)=>{
      console.log('sv: ', sv);
      return <tr key={index}>
        <td>{sv.maSV}</td>
        <td>{sv.name}</td>
        <td>{sv.phone}</td>
        <td>{sv.email}</td>
        <td>
          <button onClick={()=>this.props.suaSinhVien(sv)} className='btn btn-warning mr-3'>Sửa</button>
          <button onClick={()=>this.props.xoaSinhVien(sv.maSV)} className='btn btn-danger'>Xóa</button>

        </td>
      </tr>
    })
  }

 
  render() {
    return (
      <div className='container'>
        <table className="table">
          <thead className=' bg-dark text-light'>
            <tr>
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>
            {this.renderTable()}
            {/* render danh sách sinh viên tại đây */}
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateTopProps = (state)=>{
  return {
    mangSV: state.SinhVienReducer.mangSV,
  }
}
const mapDispatchToProps = (dispatch)=>{
  return {
    xoaSinhVien: (id)=>{
      let action = {
        type: "XOA_SINH_VIEN",
        id
      }
      dispatch(action)
    },
    suaSinhVien:(id)=>{
      let action = {
        type: "SUA_SINH_VIEN",
        id
      }
      dispatch(action);
    }
  }
}
export default connect(mapStateTopProps, mapDispatchToProps)(TableSinhVien);
