import React, { Component } from "react";
// ket noi voi redux
import { connect } from "react-redux";

class ThongTinSinhVien extends Component {
  state = {
    values: {
      maSV: "",
      name: "",
      phone: "",
      email: "",
    },
    errors: {
      maSV: "",
      name: "",
      phone: "",
      email: "",
    },
  };

  // lay thong tin nhapp tu o input
  handleChangeInput = (event) => {
    let { name, value, type } = event.target;
    let newValue = { ...this.state.values, [name]: value };
    let newError = { ...this.state.errors };

    // kiem tra nhap thong tin
    // kiem tra rong
    if (value === "") {
      newError[name] = "Vui lòng không để trống";
    } else {
      newError[name] = "";
    }

    // set state
    this.setState(
      {
        values: newValue,
        errors: newError,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  // ham dua thong tin sinh vien len redux render lai giao dien
  handleSubmit = (event) => {
    // can browser load lai trang
    event.preventDefault();
    this.props.themSinhVien(this.state.values);
  };

  render() {
    return (
      <div className="container py-5">
        <h4 className="bg-dark text-light p-3 text-left">
          Thông Tin Sinh Viên
        </h4>
        <form onSubmit={this.handleSubmit}>
          <div className="row mt-4">
            <div className="col-6 text-left">
              <span className="">Mã SV</span>
              <div className="form-group">
                <input
                  required
                  onChange={this.handleChangeInput}
                  value={this.state.values.maSV}
                  type="text"
                  className="form-control"
                  name="maSV"
                  aria-describedby="helpId"
                  placeholder="Nhập mã"
                />
                <span className="text text-danger">
                  {this.state.errors.maSV}
                </span>
              </div>
            </div>
            <div className="col-6 text-left">
              <span>Họ Tên</span>
              <div className="form-group">
                <input
                  required
                  onChange={this.handleChangeInput}
                  value={this.state.values.name}
                  type="text"
                  className="form-control"
                  name="name"
                  aria-describedby="helpId"
                  placeholder="Nhập họ tên"
                />
                <span className="text text-danger">
                  {this.state.errors.name}
                </span>
              </div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-6 text-left">
              <span className="">Số điện thoại</span>
              <div className="form-group">
                <input
                  required
                  onChange={this.handleChangeInput}
                  value={this.state.values.phone}
                  type="text"
                  className="form-control"
                  name="phone"
                  aria-describedby="helpId"
                  placeholder="Nhập số điện thoại"
                />
                <span className="text text-danger">
                  {this.state.errors.phone}
                </span>
              </div>
            </div>
            <div className="col-6 text-left">
              <span>Email</span>
              <div className="form-group">
                <input
                  required
                  onChange={this.handleChangeInput}
                  value={this.state.values.email}
                  type="email"
                  className="form-control"
                  name="email"
                  aria-describedby="helpId"
                  placeholder="Nhập email"
                />
                <span className="text text-danger">
                  {this.state.errors.email}
                </span>
              </div>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-2">
              <button className="btn btn-success">Thêm sinh viên</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themSinhVien: (sinhVien) => {
      let action = {
        type: "THEM_SINH_VIEN",
        sinhVien,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ThongTinSinhVien);
