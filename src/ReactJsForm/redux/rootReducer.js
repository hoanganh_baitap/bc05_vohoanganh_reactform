import { combineReducers } from "redux";
import SinhVienReducer from "./SinhVienReducer";

const rootReducer = combineReducers({
    SinhVienReducer
});
export default rootReducer;