let sinhVienState = {
  mangSV: [
    {
      maSV: "1",
      name: "Vo Hoang Anh",
      phone: "0938426428",
      email: "vohoanganh16@gmail.com",
    },
  ],
};

const SinhVienReducer = (state = sinhVienState, action) => {
  switch (action.type) {
    case "THEM_SINH_VIEN":
      {
        let cloneMangSV = [...state.mangSV, action.sinhVien];
        state.mangSV = cloneMangSV;
        return { ...state };
      }
      break;
    case "XOA_SINH_VIEN":
      {
        let cloneMangSV = [...state.mangSV];
        let index = cloneMangSV.findIndex((sv) => sv.maSV === action.id);
        if (index !== -1) {
          cloneMangSV.splice(index, 1);
        }
        state.mangSV = cloneMangSV;
        return { ...state };
      }
      break;
    case "SUA_SINH_VIEN": {
      let cloneMangSV = [...state.mangSV, action.id];
      state.mangSV = cloneMangSV;

      return { ...state };
    }

    default:
      return { ...state };
  }
};
export default SinhVienReducer;
